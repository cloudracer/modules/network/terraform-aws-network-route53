# Terraform Module for AWS Transit Gateway

This module will create the required AWS resources for deploying an AWS Transit Gateway which is required
to connect other VPCs or VPN connections.

Its part of a multi-stage network concept and designed to work with terraform workspaces.
For a complete network setup, you have to include additional modules.

- VPC : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpc.git
- Transit Gateway : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway.git
- VPN : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-vpn.git
- Route53 : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-route53.git
- Direct Connect : git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-direct-connect.git

***CAVE***: It's strongly required to name the terraform workspace & the shared vpc: ***shared***
This is currently a hard coded value in all modules.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Features
#### Routing
- Static routes or BGP

## Examples

    module "euc1_transit_gateway" {

        source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-network-transit-gateway.git?ref=master"

        providers = {
            aws = aws.eu-central-1
        }

        name      = local.general.name
        workspace = terraform.workspace

        enable_static_routing             = local.vpn_settings.euc1.enable_static_routing
        vpn_routing_cidrs                 = local.vpn_cidrs.euc1.datacenter
        vpn_transit_gateway_attachment_id = module.euc1_vpn.transit_gateway_attachment_id

        vpc_id          = module.euc1_vpc.vpc_id
        vpc_cidr        = local.vpc_euc1_primary_cidr
        vpc_cidr_shared = local.stages.euc1.primary.shared
        private_subnets = module.euc1_vpc.private_subnets

        tags = local.common_tags
    }

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_resolver_endpoint.outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_endpoint.r53_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_rule.corporate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule) | resource |
| [aws_route53_resolver_rule_association.corporate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule_association) | resource |
| [aws_route53_zone.cloud_region](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone.cloud_zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone.stage_zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_security_group.r53_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.r53_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.r53_inbound_egress_dns_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_inbound_egress_dns_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_inbound_ingress_dns_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_inbound_ingress_dns_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_outbound_egress_dns_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_outbound_egress_dns_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_outbound_ingress_dns_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.r53_outbound_ingress_dns_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_all_vpc_ids"></a> [all\_vpc\_ids](#input\_all\_vpc\_ids) | All VPC IDs that should get access to the Hosted Zone | `any` | n/a | yes |
| <a name="input_cloud_region"></a> [cloud\_region](#input\_cloud\_region) | R53 Hosted Zone region (creates: <region>.aws.mycompany.com) | `string` | n/a | yes |
| <a name="input_cloud_zone_name"></a> [cloud\_zone\_name](#input\_cloud\_zone\_name) | R53 Hosted Zone name for the company (e.g. aws.mycompany.com) | `string` | n/a | yes |
| <a name="input_inbound_resolver"></a> [inbound\_resolver](#input\_inbound\_resolver) | Wheter to create an inbound resolver or not | `bool` | `false` | no |
| <a name="input_local_servers"></a> [local\_servers](#input\_local\_servers) | List of local (on-prem) DNS Server IPs | `list` | `[]` | no |
| <a name="input_local_zones"></a> [local\_zones](#input\_local\_zones) | List of local (on-prem) DNS Names | `list` | `[]` | no |
| <a name="input_outbound_resolver"></a> [outbound\_resolver](#input\_outbound\_resolver) | Wheter to create an outbound resolver or not | `bool` | `false` | no |
| <a name="input_shared_vpc_private_subnets"></a> [shared\_vpc\_private\_subnets](#input\_shared\_vpc\_private\_subnets) | A map of Subnets IDs and CIDR ranges | `any` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
