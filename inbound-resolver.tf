locals {
  shared_vpc_id = { for key, vpc in var.all_vpc_ids : key => vpc if key == "shared" }
}

// R53 Security Group for Inbound Resolver
#tfsec:ignore:AWS018
resource "aws_security_group" "r53_inbound" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  vpc_id = each.value
  name   = "r53-shared-inbound"

  tags = merge(var.tags, local.tags, {
    Name = lower("sg-r53-shared-inbound")
  })
}

resource "aws_security_group_rule" "r53_inbound_ingress_dns_udp" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  security_group_id = aws_security_group.r53_inbound["shared"].id
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006
}

resource "aws_security_group_rule" "r53_inbound_ingress_dns_tcp" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  security_group_id = aws_security_group.r53_inbound["shared"].id
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006
}

resource "aws_security_group_rule" "r53_inbound_egress_dns_udp" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  security_group_id = aws_security_group.r53_inbound["shared"].id
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006 tfsec:ignore:AWS007
}

resource "aws_security_group_rule" "r53_inbound_egress_dns_tcp" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  security_group_id = aws_security_group.r53_inbound["shared"].id
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006 tfsec:ignore:AWS007
}

# Inbound Resolver
resource "aws_route53_resolver_endpoint" "r53_inbound" {
  for_each = var.inbound_resolver == true ? local.shared_vpc_id : {}

  name      = lower("r53-resolver-shared-inbound")
  direction = "INBOUND"

  security_group_ids = [aws_security_group.r53_inbound["shared"].id]

  dynamic "ip_address" {
    for_each = var.shared_vpc_private_subnets
    content {
      subnet_id = ip_address.key
      ip        = cidrhost(ip_address.value, 8)
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-network-shared-inbound")
  })
}
