## VPC
variable "all_vpc_ids" {
  description = "All VPC IDs that should get access to the Hosted Zone"
}

## Zones
variable "cloud_zone_name" {
  description = "R53 Hosted Zone name for the company (e.g. aws.mycompany.com)"
  type        = string
}

variable "cloud_region" {
  description = "R53 Hosted Zone region (creates: <region>.aws.mycompany.com)"
  type        = string
}

## Resolver
variable "inbound_resolver" {
  description = "Wheter to create an inbound resolver or not"
  type        = bool
  default     = false
}

variable "shared_vpc_private_subnets" {
  description = "A map of Subnets IDs and CIDR ranges"
}

variable "outbound_resolver" {
  description = "Wheter to create an outbound resolver or not"
  type        = bool
  default     = false
}

variable "local_zones" {
  description = "List of local (on-prem) DNS Names"
  default     = []
}

variable "local_servers" {
  description = "List of local (on-prem) DNS Server IPs"
  default     = []
}

## Other
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
