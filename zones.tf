resource "aws_route53_zone" "cloud_zone" {
  name    = "${var.cloud_zone_name}."
  comment = "Main Cloud Zone"

  dynamic "vpc" {
    for_each = var.all_vpc_ids

    content {
      vpc_id = vpc.value
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-private-zone-${var.cloud_zone_name}")
  })
}

resource "aws_route53_zone" "cloud_region" {
  name    = "${var.cloud_region}.${var.cloud_zone_name}."
  comment = "Region Cloud Zone"

  dynamic "vpc" {
    for_each = var.all_vpc_ids

    content {
      vpc_id = vpc.value
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-private-zone-${var.cloud_region}.${var.cloud_zone_name}")
  })
}

resource "aws_route53_zone" "stage_zone" {
  for_each = var.all_vpc_ids

  name    = "${each.key}.${var.cloud_region}.${var.cloud_zone_name}."
  comment = "Stage Cloud Zone"

  dynamic "vpc" {
    for_each = var.all_vpc_ids

    content {
      vpc_id = vpc.value
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-private-zone-${each.key}.${var.cloud_region}.${var.cloud_zone_name}")
  })
}
