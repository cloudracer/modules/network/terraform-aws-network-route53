// R53 Outbound Resolver
#tfsec:ignore:AWS018
resource "aws_security_group" "r53_outbound" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  vpc_id = each.value
  name   = "r53-shared-outbound"

  tags = merge(var.tags, local.tags, {
    Name = lower("sg-r53-shared-outbound")
  })
}

resource "aws_security_group_rule" "r53_outbound_ingress_dns_udp" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  security_group_id = aws_security_group.r53_outbound["shared"].id
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006
}

#tfsec:ignore:AWS006
resource "aws_security_group_rule" "r53_outbound_ingress_dns_tcp" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  security_group_id = aws_security_group.r53_outbound["shared"].id
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS018 tfsec:ignore:AWS006
}

resource "aws_security_group_rule" "r53_outbound_egress_dns_udp" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  security_group_id = aws_security_group.r53_outbound["shared"].id
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS007 tfsec:ignore:AWS006 tfsec:ignore:AWS018
}

resource "aws_security_group_rule" "r53_outbound_egress_dns_tcp" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  security_group_id = aws_security_group.r53_outbound["shared"].id
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS007 tfsec:ignore:AWS006 #tfsec:ignore:AWS018
}

resource "aws_route53_resolver_endpoint" "outbound" {
  for_each = var.outbound_resolver == true ? local.shared_vpc_id : {}

  name      = lower("r53-resolver-shared-outbound")
  direction = "OUTBOUND"

  security_group_ids = [aws_security_group.r53_outbound["shared"].id]

  dynamic "ip_address" {
    for_each = var.shared_vpc_private_subnets
    content {
      subnet_id = ip_address.key
      ip        = cidrhost(ip_address.value, 9)
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-network-shared-outbound")
  })
}

resource "aws_route53_resolver_rule" "corporate" {
  count = var.outbound_resolver == true ? length(var.local_zones) : 0

  name                 = "r53-rule-shared-outbound"
  domain_name          = var.local_zones[count.index]
  rule_type            = "FORWARD"
  resolver_endpoint_id = aws_route53_resolver_endpoint.outbound["shared"].id

  dynamic "target_ip" {
    for_each = toset(var.local_servers)

    content {
      ip = target_ip.value
    }
  }

  tags = merge(var.tags, local.tags, {
    Name = lower("r53-rule-shared-corporate")
  })
}

resource "aws_route53_resolver_rule_association" "corporate" {
  count = var.outbound_resolver == true ? length(var.local_zones) : 0

  name             = lower("r53-rule-${aws_route53_resolver_rule.corporate[count.index].id}")
  resolver_rule_id = aws_route53_resolver_rule.corporate[count.index].id
  vpc_id           = local.shared_vpc_id["shared"]
}
